<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function show()
    {
        if (view()->exists('deefald.about')) {
            return view('deefald.about')->withTitle('Hello ABOUT'); //Пятий способ
        }
    }
}
