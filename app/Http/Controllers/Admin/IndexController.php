<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;


use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function show(){

        $array = array(

            'title'=>'Laravel Project',
            'data' =>[
                'one' => 'List 1',
                'two' => 'List 2',
                'three' => 'List 3',
                'four' => 'List 4',
                'five' => 'List 5'
            ],
            'dataI' =>['List 1','List 2','List 3','List 4','List 5'],

            'bvar' => true,
            'script' =>"<script>alert('hello')</script>"

        );
        //$data = array('title'=>'Hello World','title2'=>'Hello World 2','title3'=>'Hello World 3'); Перший способ
        //return view('deefald.template',$data);

        //return view('deefald.template',['title'=>'Hello World']);//Другий способ передачі даних в вид

        //return view('deefald.template')->with('title','Hello World 2');//Третій способ передачі даних в вид

        //$view = view('deefald.template');
        //$view->with('title','Hello World');
        //$view->with('title2','Hello World 2'); //Четвертий  способ
        //$view->with('title3','Hello World 3');

        if(view()->exists('deefald.index')){

            //view()->name('deefald.template','nyview'); //Надаємо імя шаблону
            //return view()->of('nyview')->withTitle('Hello World');

//            $path = config('view.paths');
//            return view()->file($path[0].'/deefald/template.blade.php')->withTitle('Hello World');
            return view('deefald.index',$array); //Пятий способ

         // $view =  view('deefald.template',['title'=>'Hello World'])->render();
          //echo view('deefald.template',['title'=>'Hello World'])->getPath();
          //return $view;
        }
        abort(440);

    }
}
